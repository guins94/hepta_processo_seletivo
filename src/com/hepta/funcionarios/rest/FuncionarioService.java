package com.hepta.funcionarios.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//import javax.persistence.Column;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.FuncionarioDAO;

@Path("/funcionarios")
public class FuncionarioService {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private FuncionarioDAO dao;

	public FuncionarioService() {
		dao = new FuncionarioDAO();
	}

	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * Adiciona novo Funcionario
	 * 
	 * @param Funcionario: Novo Funcionario
	 * @return response 200 (OK) - Conseguiu adicionar
	 */
	@Path("/cadastro")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response FuncionarioCreate(Funcionario Funcionario) {
		Setor dummySetor = new Setor();
		Integer idSetor = 42;
		String nomeSetor = "Universo";
		
		dummySetor.setId(idSetor);
		dummySetor.setNome(nomeSetor);

		
		int idNovo = Integer.parseInt(request.getParameter("id"));
		String nomeNovo = request.getParameter("nome");
		//String setorNovo = request.getParameter("setor");
		double salarioNovo = Integer.parseInt(request.getParameter("salario"));
		String emailNovo = request.getParameter("email");
		int idadeNovo = Integer.parseInt(request.getParameter("idade"));
		
		Funcionario.setId(idNovo);
		Funcionario.setNome(nomeNovo);
		Funcionario.setSetor(dummySetor);
		Funcionario.setSalario(salarioNovo);
		Funcionario.setEmail(emailNovo);
		Funcionario.setIdade(idadeNovo);
		
		try {
			dao.save(Funcionario);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao criar funcionario").build();
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/resources/pages/cadastro-de-funcionario.js");
		try {
			dispatcher.forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		GenericEntity<Funcionario> entity = new GenericEntity<Funcionario>(Funcionario) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}

	/**
	 * Lista todos os Funcionarios
	 * 
	 * @return response 200 (OK) - Conseguiu listar
	 */
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response FuncionarioRead() {
		List<Funcionario> Funcionarios = new ArrayList<>();
		try {
			Funcionarios = dao.getAll();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Funcionarios").build();
		}

		GenericEntity<List<Funcionario>> entity = new GenericEntity<List<Funcionario>>(Funcionarios) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}

	/**
	 * Atualiza um Funcionario
	 * 
	 * @param id:          id do Funcionario
	 * @param Funcionario: Funcionario atualizado
	 * @return response 200 (OK) - Conseguiu atualizar
	 */
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PUT
	public Response FuncionarioUpdate(@PathParam("id") Integer id, Funcionario Funcionario) {
		try {
			dao.delete(id);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar funcionario").build();
		}
		
		try {
			dao.save(Funcionario);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao criar funcionario").build();
		}
		GenericEntity<Funcionario> entity = new GenericEntity<Funcionario>(Funcionario) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}

	/**
	 * Remove um Funcionario
	 * 
	 * @param id: id do Funcionario
	 * @return response 200 (OK) - Conseguiu remover
	 */
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@DELETE
	public Response FuncionarioDelete(@PathParam("id") Integer id) {
		try {
			dao.delete(id);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar funcionario").build();
		}

		GenericEntity<Integer> entity = new GenericEntity<Integer>(id) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}

}
